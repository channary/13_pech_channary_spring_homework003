package kgsa.springhomework003.repository;

import kgsa.springhomework003.model.entity.Author;
import kgsa.springhomework003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {
    @Select("SELECT * FROM authors")
    @Results(id = "authorMap", value = {
            @Result(property = "authorId", column = "author_id"),
            @Result(property = "authorName", column = "author_name"),
            @Result(property = "gender", column = "gender"),
    })
    List<Author> findAllAuthor();

    @Select("SELECT * FROM authors where author_id = #{authorId}")
    @ResultMap("authorMap")
    Author getAuthorById(Integer authorId);
    @Delete("DELETE FROM authors where author_id = #{id}")
    Boolean deleteAuthorById(@Param("id") Integer authorId);


    @Select("INSERT INTO authors (author_name,gender) VALUES(#{request.authorName}, #{request.gender}) " +
            "RETURNING author_id")
    Integer insertAuthor(@Param("request") AuthorRequest authorRequest);

    @Select("UPDATE authors " +
            "SET author_name = #{request.authorName}," +
            "gender = #{request.gender} " +
            "WHERE author_id = #{authorId} " +
            "RETURNING author_id" )
    Integer updateAuthor(@Param("request") AuthorRequest request, Integer authorId);


}
