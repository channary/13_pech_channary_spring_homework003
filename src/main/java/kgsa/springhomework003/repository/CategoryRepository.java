package kgsa.springhomework003.repository;

import kgsa.springhomework003.model.entity.Category;
import kgsa.springhomework003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @Select("SELECT * FROM categories")
    @Results(id = "categoryMap", value = {
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    List<Category> findAllCategory();

    @Select("SELECT * FROM categories where category_id = #{categoryId}")
    @ResultMap("categoryMap")
    Category getCategoryById(Integer categoryId);


    @Select("INSERT INTO categories (category_name) VALUES(#{request.categoryName}) " +
            "RETURNING category_id")
    Integer insertCategory(@Param("request") CategoryRequest categoryRequest);

    @Select("UPDATE categories " +
            "SET category_name = #{request.categoryName} " +
            "WHERE category_id = #{categoryId} " +
            "RETURNING category_id" )
    Integer updateCategory(@Param("request") CategoryRequest request, Integer categoryId);
        @Delete("DELETE FROM categories where category_id = #{id}")
    Boolean deleteCategoryById(@Param("id") Integer categoryId);

    @Select("SELECT c.category_id, c.category_name FROM book_details bd " +
            "INNER JOIN categories c ON c.category_id = bd.category_id " +
            "WHERE bd.book_id = #{bookId}")
    @ResultMap("categoryMap")
    List<Category> getCategoryByIdInnerJoinBookDetails(Integer bookId);

}
