package kgsa.springhomework003.repository;

import kgsa.springhomework003.model.entity.Book;
import kgsa.springhomework003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    @Select("SELECT * FROM books")
    @Results(id = "bookMap",value = {
            @Result(property = "bookId", column = "book_id"),
            @Result(property = "publishedDate", column = "published_date"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author_id",
                    one = @One(select = "kgsa.springhomework003.repository.AuthorRepository.getAuthorById")
            ),
            @Result(property = "categories", column = "book_id",
                    many = @Many(select = "kgsa.springhomework003.repository.CategoryRepository.getCategoryByIdInnerJoinBookDetails")
            )
    })
    List<Book> findAllBook();

    @Select("SELECT * FROM books WHERE book_id = #{bookId}")
    @ResultMap("bookMap")
    Book getBookById(Integer bookId);

    @Delete("DELETE FROM books WHERE book_id = #{id}")
    Boolean deleteBookById(@Param("id") Integer bookId);


    @Select("INSERT INTO books (title, author_id) " +
            "VALUES (#{request.title}, #{request.authorId}) " +
            "RETURNING book_id")
    @Result(property = "bookId", column = "book_id")
    Integer storeNewBook(@Param("request") BookRequest bookRequest);


    @Insert("INSERT INTO book_details ( book_id, category_id) " +
            "VALUES (#{bookId}, #{categoryId})")
    void saveBookByCategoryId(Integer categoryId, Integer bookId);


    @Select("UPDATE books " +
            "SET author_id = #{request.authorId}, " +
            "title = #{request.title} " +
            "WHERE book_id = #{bookId} " +
            "RETURNING book_id")
    Integer updateBookId(@Param("request") BookRequest bookRequest, Integer bookId);

    @Select("UPDATE book_details " +
            "SET book_id = #{bookId}, " +
            "category_id = #{categoryId} " +
            "WHERE book_id = #{bookId} ")
    Integer updateCategoryByBookId(Integer bookId, Integer categoryId);

}
