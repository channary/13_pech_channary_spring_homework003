package kgsa.springhomework003.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorResponse<T> {

    private Timestamp timestamp;
    private Integer success;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;

}
