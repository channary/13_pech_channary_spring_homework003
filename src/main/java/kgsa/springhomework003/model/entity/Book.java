package kgsa.springhomework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Book {


    private Integer bookId;
    private String title;
    private Timestamp publishedDate;
    private List<Category> categories = new ArrayList<>();
    private Author author;


}
