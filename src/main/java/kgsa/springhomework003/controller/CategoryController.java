package kgsa.springhomework003.controller;


import kgsa.springhomework003.model.entity.Category;
import kgsa.springhomework003.model.request.CategoryRequest;
import kgsa.springhomework003.model.response.CategoryResponse;
import kgsa.springhomework003.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllCategory(){
        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .payload(categoryService.getAllCategory())
                .message("Fetch all Category is successfully !!")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<CategoryResponse<Category>> getCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryById(categoryId))
                .message("This Category was found !!")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response);
    }

    @PostMapping("/add-new-category")
    public ResponseEntity<CategoryResponse<Category>> addNewCategory(@RequestBody CategoryRequest categoryRequest){
        Integer addCategoryId = categoryService.addNewCategory(categoryRequest);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryById(addCategoryId))
                .message("Insert Category is successfully")
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .success(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update-category-by-id/{id}")
    public ResponseEntity<CategoryResponse<Category>> updateCategoryId(
            @RequestBody CategoryRequest categoryRequest, @PathVariable("id") Integer categoryId)
    {
        Integer idCategoryUpdate = categoryService.updateCategory(categoryRequest,categoryId);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .payload(categoryService.getCategoryById(categoryId))
                .message("Update Category is successfully")
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .success(HttpStatus.OK.value())
                .build();
        return  ResponseEntity.ok(response);
    }
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<CategoryResponse<String>> deleteCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<String> response  = CategoryResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .message("This Author was deleted !!")
                    .success(HttpStatus.OK.value())
                    .build();
        return ResponseEntity.ok(response);
    }
}
