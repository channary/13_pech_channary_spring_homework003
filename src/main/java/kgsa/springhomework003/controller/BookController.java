package kgsa.springhomework003.controller;

import kgsa.springhomework003.model.entity.Book;
import kgsa.springhomework003.model.request.BookRequest;
import kgsa.springhomework003.model.response.BookResponse;
import kgsa.springhomework003.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
@RequestMapping("/api/v1")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/books")
    public ResponseEntity<BookResponse<List<Book>>> getAllBook() {
        BookResponse<List<Book>> response = BookResponse.<List<Book>>builder()
                .payload(bookService.getAllBook())
                .message("Fetch all Category is successfully !!")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BookResponse<Book>> getBookById(@PathVariable("id") Integer BookId) {
        BookResponse<Book> response = BookResponse.<Book>builder()
                .payload(bookService.getBookById(BookId))
                .message("This Book was found !!")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }



    @PostMapping("/books")
    public ResponseEntity<BookResponse<Book>> addNewBook(@RequestBody BookRequest bookRequest){
        System.out.println("this is author " + bookRequest.getTitle());
        System.out.println("this is author " + bookRequest.getAuthorId());
        Integer storeBookId = bookService.addNewBook(bookRequest);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .payload(bookService.getBookById(storeBookId))
                .message("Insert data successfully")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);

    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BookResponse<Book>> updateBook(
            @RequestBody BookRequest bookRequest,@PathVariable("id") Integer bookId)
    {
        Integer BookIdUpdate = bookService.updateBook(bookRequest,bookId);
        BookResponse<Book> response = BookResponse.<Book>builder()
                    .payload(bookService.getBookById(bookId))
                    .message("Book update successfully")
                    .success(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<BookResponse<String>> deleteBookById(
            @PathVariable("id") Integer bookId) {
        BookResponse<String> response = null;
        if (bookService.deleteBookById(bookId) == true) {
            response = BookResponse.<String>builder()
                    .message("Delete Book Successfully")
                    .success(HttpStatus.OK.value())
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
        }

}
