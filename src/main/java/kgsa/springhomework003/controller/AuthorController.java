package kgsa.springhomework003.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import kgsa.springhomework003.model.entity.Author;
import kgsa.springhomework003.model.request.AuthorRequest;
import kgsa.springhomework003.model.response.AuthorResponse;
import kgsa.springhomework003.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
@RequestMapping("/api/v1")

public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    @GetMapping("/authors")
    public ResponseEntity<AuthorResponse<List<Author>>> getAllAuthor(){
        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .payload(authorService.getAllAuthor())
                .message("Fetch all author is successfully !!")
                .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/authors/{id}")
    public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .payload(authorService.getAuthorById(authorId))
                    .message("This Author was found !!")
                    .success(HttpStatus.OK.value())
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
            return  ResponseEntity.ok(response);
    }

    @DeleteMapping("/authors/{id}")
    public ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<String> response = null;
        if (authorService.deleteAuthorById(authorId) == true) {
                    response = AuthorResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .message("This Author was deleted !!")
                    .success(HttpStatus.OK.value())
                    .build();
        }
            return ResponseEntity.ok(response);
    }

    @PostMapping("/authors")
    public ResponseEntity<AuthorResponse<Author>> addNewAuthor(@RequestBody AuthorRequest authorRequest){
        Integer addAuthorId = authorService.addNewAuthor(authorRequest);
            AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .payload(authorService.getAuthorById(addAuthorId))
                    .message("Insert Author is successfully")
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .success(HttpStatus.OK.value())
                    .build();
            return ResponseEntity.ok(response);
    }

    @PutMapping("/authors/{id}")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorId(
            @RequestBody AuthorRequest authorRequest, @PathVariable("id") Integer authorId)
    {
        Integer idAuthorUpdate = authorService.updateAuthor(authorRequest,authorId);
        AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                    .payload(authorService.getAuthorById(authorId))
                    .message("Update Author is successfully")
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .success(HttpStatus.OK.value())
                    .build();
            return  ResponseEntity.ok(response);
    }

}
