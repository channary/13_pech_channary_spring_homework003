package kgsa.springhomework003.exception;

public class AuthorEmptyException extends RuntimeException{
    public AuthorEmptyException(String emptyMessage) {
        super(emptyMessage);
    }
}
