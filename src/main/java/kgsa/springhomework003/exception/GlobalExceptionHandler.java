package kgsa.springhomework003.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(AuthorNotFoundException.class)
    ProblemDetail handleAuthorNotFoundException(AuthorNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Author not Found");
        problemDetail.setStatus(404);
        problemDetail.setInstance(URI.create("/api/v1/author"));
        return problemDetail;
    }

    @ExceptionHandler(AuthorEmptyException.class)
    ProblemDetail handleAuthorEmptyException(AuthorEmptyException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Author is empty");
        problemDetail.setStatus(200);
        problemDetail.setInstance(URI.create("/api/v1/author"));
        return problemDetail;
    }


    @ExceptionHandler(CategoryEmptyException.class)
    ProblemDetail handleCategoryEmptyException(CategoryEmptyException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Category is empty");
        problemDetail.setStatus(200);
        problemDetail.setInstance(URI.create("/api/v1/category"));
        return problemDetail;
    }


    @ExceptionHandler(CategoryNotFoundException.class)
    ProblemDetail handleCategoryNotFoundException(CategoryNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Category not found");
        problemDetail.setStatus(404);
        problemDetail.setInstance(URI.create("/api/v1/category"));
        return problemDetail;
    }

    @ExceptionHandler(BookEmptyException.class)
    ProblemDetail handleBookEmptyException(BookEmptyException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/empty"));
        problemDetail.setTitle("Book is empty");
        problemDetail.setStatus(200);
        problemDetail.setInstance(URI.create("/api/v1/book"));
        return problemDetail;
    }

    @ExceptionHandler(BookNotFoundException.class)
    ProblemDetail handleBookNotFoundException(BookNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("http://localhost:8080/errors/not-found"));
        problemDetail.setTitle("Book not found");
        problemDetail.setStatus(404);
        problemDetail.setInstance(URI.create("/api/v1/book"));
        return problemDetail;
    }
}
