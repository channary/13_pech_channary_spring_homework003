package kgsa.springhomework003.exception;

public class BookEmptyException extends RuntimeException{
    public BookEmptyException(String message) {
        super(message);
    }
}
