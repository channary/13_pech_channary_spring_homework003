package kgsa.springhomework003.exception;

public class CategoryEmptyException extends RuntimeException{
    public CategoryEmptyException(String emptyCategoryMessage) {
        super(emptyCategoryMessage);
    }
}
