package kgsa.springhomework003.service;

import kgsa.springhomework003.model.entity.Category;
import kgsa.springhomework003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();

    Category getCategoryById(Integer CategoryId);

    Boolean deleteCategoryById(Integer CategoryId);

    Integer addNewCategory(CategoryRequest categoryRequest);

    Integer updateCategory(CategoryRequest categoryRequest, Integer CategoryId);
}
