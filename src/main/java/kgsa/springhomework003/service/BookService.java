package kgsa.springhomework003.service;

import kgsa.springhomework003.model.entity.Book;
import kgsa.springhomework003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();

    Book getBookById(Integer BookId);

    Integer addNewBook(BookRequest bookRequest);

    Integer updateBook(BookRequest bookRequest,Integer bookId);

    Boolean deleteBookById(Integer bookId);
}
