package kgsa.springhomework003.service.serviceImp;

import kgsa.springhomework003.exception.AuthorEmptyException;
import kgsa.springhomework003.exception.AuthorNotFoundException;
import kgsa.springhomework003.model.entity.Author;
import kgsa.springhomework003.model.request.AuthorRequest;
import kgsa.springhomework003.repository.AuthorRepository;
import kgsa.springhomework003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<Author> getAllAuthor() {
        List<Author> allAuthor = authorRepository.findAllAuthor();
        if(allAuthor.isEmpty()){
            throw new AuthorEmptyException("There is no Author");
        }
        return authorRepository.findAllAuthor();
    }
    @Override
    public Author getAuthorById(Integer authorId) {
        Author author = authorRepository.getAuthorById(authorId);
        if(author == null){
            throw new AuthorNotFoundException("Author with id: " + authorId + " not found");
        }
        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public Boolean deleteAuthorById(Integer authorId) {
        Boolean deleteId = authorRepository.deleteAuthorById(authorId);
        if (!deleteId){
            throw new AuthorNotFoundException("Author with id: " + authorId + " not found");
        }
        return true;
    }

    @Override
    public Integer addNewAuthor(AuthorRequest authorRequest) {
        String authorGender = authorRequest.getGender();
        String authorName = authorRequest.getAuthorName();

        if(!(authorGender.equalsIgnoreCase("female") || authorGender.equalsIgnoreCase("male")))
        {
            throw new AuthorNotFoundException("Gender field can be only 'male' or 'female'");
        }
        else if (authorName.isBlank()) {
            throw new AuthorNotFoundException("Author Name field can not be blank");
        }
        return authorRepository.insertAuthor(authorRequest);
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        if (!(authorRepository.getAuthorById(authorId) != null  &&
                (authorRequest.getGender().equalsIgnoreCase("female")
                        || authorRequest.getGender().equalsIgnoreCase("male"))
        )){
            throw new AuthorNotFoundException("Gender field can be only 'male' or 'female'");
        }
        return authorRepository.updateAuthor(authorRequest,authorId);
    }
}
