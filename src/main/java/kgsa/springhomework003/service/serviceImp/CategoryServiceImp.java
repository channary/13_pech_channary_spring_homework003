package kgsa.springhomework003.service.serviceImp;


import kgsa.springhomework003.exception.CategoryNotFoundException;
import kgsa.springhomework003.exception.CategoryEmptyException;
import kgsa.springhomework003.model.entity.Category;
import kgsa.springhomework003.model.request.CategoryRequest;
import kgsa.springhomework003.repository.CategoryRepository;
import kgsa.springhomework003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    // get all category
    @Override
    public List<Category> getAllCategory() {
        List<Category> allCategory = categoryRepository.findAllCategory();
        if(allCategory.isEmpty()){
            throw new CategoryEmptyException("There is no Category");
        }
        return allCategory;
    }

    @Override
    public Category getCategoryById(Integer categoryId) {
        Category category = categoryRepository.getCategoryById(categoryId);
        if (category == null){
            throw new CategoryNotFoundException("Category with id: " + categoryId + " not found");
        }
        return category;
    }

    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest) {
        String categoryName = categoryRequest.getCategoryName();
        if (categoryName.isBlank()){
            throw new CategoryNotFoundException("Field CategoryName Can't be blank");
        }
        return categoryRepository.insertCategory(categoryRequest);
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId) {
        String categoryName = categoryRequest.getCategoryName();
        if(categoryRepository.getCategoryById(categoryId) == null){
            throw new CategoryNotFoundException("Id not found");
        }if (categoryName.isBlank()){
            throw new CategoryNotFoundException("Category Name can't be blank");
        }
        return categoryRepository.updateCategory(categoryRequest,categoryId);
    }


    @Override
    public Boolean deleteCategoryById(Integer categoryId) {
        Boolean deleteId = categoryRepository.deleteCategoryById(categoryId);
        if (!deleteId){
            throw new CategoryNotFoundException("Category with id: " + categoryId + " not found");
        }
        return true;
    }
}
