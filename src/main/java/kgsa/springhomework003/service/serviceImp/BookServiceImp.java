package kgsa.springhomework003.service.serviceImp;

import kgsa.springhomework003.exception.AuthorNotFoundException;
import kgsa.springhomework003.exception.BookEmptyException;
import kgsa.springhomework003.exception.BookNotFoundException;
import kgsa.springhomework003.exception.CategoryNotFoundException;
import kgsa.springhomework003.model.entity.Author;
import kgsa.springhomework003.model.entity.Book;
import kgsa.springhomework003.model.entity.Category;
import kgsa.springhomework003.model.request.BookRequest;
import kgsa.springhomework003.repository.AuthorRepository;
import kgsa.springhomework003.repository.BookRepository;
import kgsa.springhomework003.repository.CategoryRepository;
import kgsa.springhomework003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService {
    private final AuthorRepository authorRepository;
    private final CategoryRepository categoryRepository;
    private final BookRepository bookRepository;

    public BookServiceImp(AuthorRepository authorRepository, CategoryRepository categoryRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.categoryRepository = categoryRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBook() {
        List<Book> allBook = bookRepository.findAllBook();
        if (allBook.isEmpty()){
            throw new BookEmptyException("There is no Book");
        }
        return allBook;
    }

    @Override
    public Book getBookById(Integer bookId) {
        Book book = bookRepository.getBookById(bookId);
        if (book == null){
            throw new BookNotFoundException("Book with id: " + bookId + " not found");
        }
        return book;
    }

    @Override
    public Integer addNewBook(BookRequest bookRequest) {
        Author author = authorRepository.getAuthorById(bookRequest.getAuthorId());

        if (bookRequest.getTitle().isBlank()){
            throw new BookNotFoundException("Title can't be blank");
        }
        if(author == null){
            throw new BookNotFoundException("authorId can't found ");
        }
        else {
            for (Integer categoryId: bookRequest.getCategoryId()){
                if((categoryRepository.getCategoryById(categoryId) == null)){
                    throw new CategoryNotFoundException("Category id " + categoryId + " not found");
                }
            }
        }

        Integer storeBookId = bookRepository.storeNewBook(bookRequest);
        for (Integer categoryId: bookRequest.getCategoryId()){
            if((categoryRepository.getCategoryById(categoryId) == null)){
                throw new CategoryNotFoundException("Category id " + categoryId + " not found");
            }
            bookRepository.saveBookByCategoryId(categoryId,storeBookId);
        }
        return storeBookId;
    }

    @Override
    public Integer updateBook(BookRequest bookRequest, Integer bookId) {

        if (bookRequest.getTitle().isBlank()){
            throw new BookNotFoundException("Title can't be blank");
        }
        if(authorRepository.getAuthorById(bookRequest.getAuthorId()) == null) {
            throw new BookNotFoundException("authorId can't found ");
        }
        for (Integer categoryId: bookRequest.getCategoryId()){
            if((categoryRepository.getCategoryById(categoryId) == null)){
                throw new CategoryNotFoundException("Category id " + categoryId + " not found");
            }
            bookRepository.updateCategoryByBookId(bookId,categoryId);
        }
        Integer updateBookId = bookRepository.updateBookId(bookRequest,bookId);
        return updateBookId;
    }

    @Override
    public Boolean deleteBookById(Integer bookId) {
        Boolean deleteId = bookRepository.deleteBookById(bookId);
        if (!deleteId){
            throw new AuthorNotFoundException("Author with id: " + bookId + " not found");
        }
        return true;
    }
}
