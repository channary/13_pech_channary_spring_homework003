package kgsa.springhomework003.service;

import kgsa.springhomework003.model.entity.Author;
import kgsa.springhomework003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();

    Author getAuthorById(Integer authorId);

    Boolean deleteAuthorById(Integer authorId);


    Integer addNewAuthor(AuthorRequest authorRequest);

    Integer updateAuthor(AuthorRequest authorRequest, Integer authorId);

}
